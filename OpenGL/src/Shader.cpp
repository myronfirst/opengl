#include "Shader.h"

#include "Error.h"
#include "gtc/type_ptr.hpp"

#include <fstream>
#include <streambuf>

std::string ParseShader(const std::string &filePath) {
    std::ifstream fileStream(filePath);
    if (!fileStream.is_open()) ThrowError("ParseShader: Wrong filePath " + filePath);
    auto begin = std::istreambuf_iterator<char>(fileStream);
    auto end = std::istreambuf_iterator<char>();
    std::string str(begin, end);
    return str;
}

GLuint CompileShader(GLenum type, const std::string &source) {
    GLuint shaderId = glCreateShader(type);
    const char *src = source.c_str();
    glShaderSource(shaderId, 1, &src, nullptr);
    glCompileShader(shaderId);

    GLint shaderCompiled;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &shaderCompiled);
    if (shaderCompiled != GL_TRUE) {
        GLsizei length = 0;
        GLchar msg[1024];
        glGetShaderInfoLog(shaderId, 1024, &length, msg);
        ThrowError("glCompileShader: " + std::string(msg));
    }

    return shaderId;
}

GLuint CreateProgram(const std::string &vertexShader, const std::string &fragmentShader) {
    GLuint program = glCreateProgram();
    if (!program) ThrowError("glCreateProgram");
    GLuint vs = CompileShader(GL_VERTEX_SHADER, vertexShader);
    GLuint fs = CompileShader(GL_FRAGMENT_SHADER, fragmentShader);

    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    GLint programLinked;
    glGetProgramiv(program, GL_LINK_STATUS, &programLinked);
    if (programLinked != GL_TRUE) {
        GLsizei length = 0;
        GLchar msg[1024];
        glGetProgramInfoLog(program, 1024, &length, msg);
        ThrowError("glLinkProgram: " + std::string(msg));
    }

    glValidateProgram(program);

    GLint programValidated;
    glGetProgramiv(program, GL_VALIDATE_STATUS, &programValidated);
    if (programValidated != GL_TRUE) {
        GLsizei length = 0;
        GLchar msg[1024];
        glGetProgramInfoLog(program, 1024, &length, msg);
        ThrowError("glValidateProgram: " + std::string(msg));
    }

    glDeleteShader(vs);
    glDeleteShader(fs);

    return program;
}

Shader::Shader(const std::string &vertexShaderPath, const std::string &fragmentShaderPath) {
    std::string vertexSource = ParseShader(vertexShaderPath);
    std::string fragmentSource = ParseShader(fragmentShaderPath);
    GLuint vertexShaderId = CompileShader(GL_VERTEX_SHADER, vertexSource);
    GLuint fragmentShaderId = CompileShader(GL_FRAGMENT_SHADER, fragmentSource);

    programId = glCreateProgram();
    if (!programId) ThrowError("glCreateProgram");

    glAttachShader(programId, vertexShaderId);
    glAttachShader(programId, fragmentShaderId);
    glLinkProgram(programId);

    GLint programLinked;
    glGetProgramiv(programId, GL_LINK_STATUS, &programLinked);
    if (programLinked != GL_TRUE) {
        GLsizei length = 0;
        GLchar msg[1024];
        glGetProgramInfoLog(programId, 1024, &length, msg);
        ThrowError("glLinkProgram: " + std::string(msg));
    }

    glValidateProgram(programId);

    GLint programValidated;
    glGetProgramiv(programId, GL_VALIDATE_STATUS, &programValidated);
    if (programValidated != GL_TRUE) {
        GLsizei length = 0;
        GLchar msg[1024];
        glGetProgramInfoLog(programId, 1024, &length, msg);
        ThrowError("glValidateProgram: " + std::string(msg));
    }

    glDeleteShader(vertexShaderId);
    glDeleteShader(fragmentShaderId);
}

Shader::~Shader() {
    glDeleteProgram(programId);
}

void Shader::Bind() const {
    glUseProgram(programId);
}

void Shader::Unbind() const {
    glUseProgram(0);
}

void Shader::SetUniform1i(const std::string &name, int v0) {
    glUniform1i(GetUniformLocation(name), v0);
}

void Shader::SetUniform4f(const std::string &name, float v0, float v1, float v2, float v3) {
    glUniform4f(GetUniformLocation(name), v0, v1, v2, v3);
}

void Shader::SetUniformMatrix4f(const std::string &name, const glm::mat4 &matrix) {
    glUniformMatrix4fv(GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(matrix));
}

int Shader::GetUniformLocation(const std::string &name) const {
    int location = glGetUniformLocation(programId, name.c_str());
    if (location == -1) ThrowError("glGetUniformLocation");
    return location;
}
