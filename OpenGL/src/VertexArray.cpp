#include "VertexArray.h"

VertexArray::VertexArray() {
    glGenVertexArrays(1, &vertexArrayObjectId);
}

VertexArray::~VertexArray() {
    glDeleteVertexArrays(1, &vertexArrayObjectId);
}

void VertexArray::Bind() const {
    glBindVertexArray(vertexArrayObjectId);
}

void VertexArray::Unbind() const {
    glBindVertexArray(0);
}

void VertexArray::AddVertexBuffer(const VertexBuffer &buffer, const VertexBufferLayout &bufferLayout) {
    buffer.Bind();
    Bind();
    for (const auto &el : bufferLayout.GetElements()) {
        glVertexAttribPointer(el.index, el.size, el.type, el.normalized, el.stride, el.offset);
        glEnableVertexAttribArray(el.index);
    }
    Unbind();
    buffer.Unbind();
}

void VertexArray::AddIndexBuffer(const IndexBuffer &buffer) {
    Bind();
    buffer.Bind();
    Unbind();
    buffer.Unbind();
}