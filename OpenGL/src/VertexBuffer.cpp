#include "VertexBuffer.h"

#include <vector>

VertexBuffer::VertexBuffer(const std::vector<Vertex> &bufferData) {
    glGenBuffers(1, &vertexBufferObjectId);
    Bind();
    glBufferData(GL_ARRAY_BUFFER, bufferData.size() * sizeof(Vertex), bufferData.data(), GL_STATIC_DRAW);
    Unbind();
    size = bufferData.size();
}

VertexBuffer::~VertexBuffer() {
    glDeleteBuffers(1, &vertexBufferObjectId);
}

void VertexBuffer::Bind() const{
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectId);
}

void VertexBuffer::Unbind() const{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}