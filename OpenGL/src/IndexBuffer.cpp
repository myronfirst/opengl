#include "IndexBuffer.h"

#include <vector>

IndexBuffer::IndexBuffer(const std::vector<unsigned> &bufferData) {
    glGenBuffers(1, &indexBufferObjectId);
    Bind();
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, bufferData.size() * sizeof(unsigned), bufferData.data(), GL_STATIC_DRAW);
    Unbind();
    size = bufferData.size();
}

IndexBuffer::~IndexBuffer() {
    glDeleteBuffers(1, &indexBufferObjectId);
}

void IndexBuffer::Bind() const{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObjectId);
}

void IndexBuffer::Unbind() const{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}