#include "Texture.h"

#include "Error.h"

Texture::Texture(const std::string &texturePath, unsigned _textureSlot) : textureSlot{ _textureSlot } {
    stbi_set_flip_vertically_on_load(1);
    unsigned char *imageBuffer = nullptr;
    int width, height, bpp;
    imageBuffer = stbi_load(texturePath.c_str(), &width, &height, &bpp, 4);
    if (!imageBuffer) ThrowError("stbi_load");

    glGenTextures(1, &textureId);
    Bind();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageBuffer);
    Unbind();

    stbi_image_free(imageBuffer);
}

Texture::~Texture() {
    glDeleteTextures(1, &textureId);
}

void Texture::Bind() const {
    glActiveTexture(GL_TEXTURE0 + textureSlot);
    glBindTexture(GL_TEXTURE_2D, textureId);
}

void Texture::Unbind() const {
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE0);
}
