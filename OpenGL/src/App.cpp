#include "App.h"

#include "Error.h"
#include "Shader.h"
#include "Texture.h"
#include "Vertex.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"

#include "glm.hpp"
#include "gtc/matrix_transform.hpp"

#include <cassert>
#include <iostream>
#include <string>
#include <vector>

App::App() {
    if (!glfwInit()) ThrowError("glfwInit");
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_REFRESH_RATE, GLFW_DONT_CARE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);    //does not automatically create Vertex Array Object
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Hello World OpenGL", NULL, NULL);
    if (!window) ThrowError("glfwCreateWindow");
    //GLFWvidmode vidMode;
    //glfwGetDesktopMode(&vidMode);
    //windowWidth = vidMode.Width;
    //windowHeight = vidMode.Height;
    glfwGetWindowSize(window, &windowWidth, &windowHeight);
    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    if (glewInit() != GLEW_OK) ThrowError("glewInit");
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(MessageCallback, 0);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;
}

App::~App() {
    glfwTerminate();
}

void App::Run() {
    std::vector<Vertex> vertices{
        { { -0.5f, -0.5f, 0.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f } },
        { { 0.5f, -0.5f, 0.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f, 1.0f }, { 1.0f, 0.0f } },
        { { -0.5f, 0.5f, 0.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f, 1.0f }, { 0.0f, 1.0f } },
        { { 0.5f, 0.5f, 0.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f } }
    };
    std::vector<unsigned> vertexIndices{
        0, 1, 2,
        2, 1, 3
    };

    Shader shader(vertexShaderPath, fragmentShaderPath);
    Texture texture(texturePath, 0);
    shader.Bind();
    shader.SetUniform1i("u_texture", 0);

    glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
    glm::vec3 axis = glm::vec3(1.0f, 0.0f, 0.0f);
    model = glm::rotate(model, glm::radians(-55.0f), axis);

    glm::mat4 view = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -3.0f));

    float near = 0.1f;
    float far = 100.0f;

    float left = -2.0f;
    float right = 2.0f;
    float bottom = -1.5f;
    float top = 1.5f;
    //glm::mat4 proj = glm::ortho(left, right, bottom, top, near, far);
    float fov = 45.0f;
    //float width = glm::abs(left - right);
    float width = static_cast<float>(GetWindowWidth());
    //float height = glm::abs(bottom - top);
    float height = static_cast<float>(GetWindowHeight());
    glm::mat4 proj = glm::perspective(glm::radians(fov), width / height, near, far);
    glm::mat4 mvp = proj * view * model;
    shader.SetUniformMatrix4f("u_mvp", mvp);

    VertexBufferLayout vbl;
    vbl.Push({ 3, GL_FLOAT, false, sizeof(Vertex), 0 });
    vbl.Push({ 3, GL_FLOAT, false, sizeof(Vertex), sizeof(Vertex::position) });
    vbl.Push({ 3, GL_FLOAT, false, sizeof(Vertex), sizeof(Vertex::position) + sizeof(Vertex::normal) });
    vbl.Push({ 3, GL_FLOAT, false, sizeof(Vertex), sizeof(Vertex::position) + sizeof(Vertex::normal) + sizeof(Vertex::color) });
    VertexBuffer vbo(vertices);
    IndexBuffer ibo(vertexIndices);
    VertexArray vao;
    vao.AddVertexBuffer(vbo, vbl);
    vao.AddIndexBuffer(ibo);

    shader.Unbind();
    texture.Unbind();
    vao.Unbind();
    vbo.Unbind();
    ibo.Unbind();
    while (!glfwWindowShouldClose(window)) {
        glClear(GL_COLOR_BUFFER_BIT);

        shader.Bind();
        texture.Bind();

        vao.Bind();
        glDrawElements(GL_TRIANGLES, ibo.GetSize(), GL_UNSIGNED_INT, nullptr);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
}
