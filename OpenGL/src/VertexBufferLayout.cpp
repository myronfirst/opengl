#include "VertexBufferLayout.h"

VertexBufferElement::VertexBufferElement(unsigned _size, unsigned _type, bool _normalized, int _stride, unsigned _offset) {
    index = 0;
    size = _size;
    type = _type;
    normalized = _normalized ? 1 : 0;
    stride = _stride;
    offset = reinterpret_cast<const GLvoid *>(_offset);
}

void VertexBufferLayout::Push(VertexBufferElement element) {
    element.index = static_cast<unsigned>(elements.size());
    elements.push_back(element);
};

const std::vector<VertexBufferElement>& VertexBufferLayout::GetElements() const {
    return elements;
}