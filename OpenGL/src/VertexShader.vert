#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;
layout(location = 2) in vec4 color;
layout(location = 3) in vec2 texCoord;

out vec4 v_color;
out vec2 v_texCoord;

uniform mat4 u_mvp;

void main() 
{
	v_color = color;
	v_texCoord = texCoord;
	gl_Position = u_mvp * position;
};