#ifndef VERTEX_H
#define VERTEX_H

struct Vertex {
    float position[3];
    float normal[3];
    float color[4];
    float texture[2];
};

#endif
