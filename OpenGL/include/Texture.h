#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glew.h>

#include "stb_image.h"

#include <string>

class Texture {
private:
    unsigned textureId;
    unsigned textureSlot;

public:
    Texture(const std::string &texturePath, unsigned _textureSlot = 0);
    ~Texture();
    void Bind() const;
    void Unbind() const;
};
#endif