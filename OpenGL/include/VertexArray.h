#ifndef VERTEX_ARRAY_H
#define VERTEX_ARRAY_H

#include "GL/glew.h"

#include "IndexBuffer.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"

class VertexArray {
private:
    GLuint vertexArrayObjectId;

public:
    VertexArray();
    ~VertexArray();
    void Bind() const;
    void Unbind() const;
    void AddVertexBuffer(const VertexBuffer &buffer, const VertexBufferLayout &bufferLayout);
    void AddIndexBuffer(const IndexBuffer &buffer);
};

#endif