#ifndef VERTEX_BUFFER_H
#define VERTEX_BUFFER_H

#include "GL/glew.h"
#include "Vertex.h"

#include <vector>

class VertexBuffer {
private:
    GLuint vertexBufferObjectId;
    unsigned size;

public:
    VertexBuffer(const std::vector<Vertex> &bufferData);
    ~VertexBuffer();
    void Bind() const;
    void Unbind() const;
    unsigned GetSize() const { return size; };
};

#endif