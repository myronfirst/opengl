#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>

#include "glm.hpp"

#include <string>

std::string ParseShader(const std::string &filePath);
GLuint CompileShader(GLenum type, const std::string &source);
GLuint CreateProgram(const std::string &vertexShader, const std::string &fragmentShader);

class Shader {
private:
    GLuint programId;

public:
    Shader(const std::string &vertexShaderPath, const std::string &fragmentShaderPath);
    ~Shader();
    void Bind() const;
    void Unbind() const;
    void SetUniform1i(const std::string &name, int v0);
    void SetUniform4f(const std::string &name, float v0, float v1, float v2, float v3);
    void SetUniformMatrix4f(const std::string &name, const glm::mat4 &matrix);
    int GetUniformLocation(const std::string &name) const;
};

#endif