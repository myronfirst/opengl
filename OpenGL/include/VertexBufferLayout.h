#ifndef VERTEX_BUFFER_LAYOUT_H
#define VERTEX_BUFFER_LAYOUT_H

#include "GL/glew.h"

#include <vector>

struct VertexBufferElement {
    VertexBufferElement(unsigned _size, unsigned _type, bool _normalized, int _stride, unsigned _offset);
    GLuint index;
    GLint size;
    GLenum type;
    GLboolean normalized;
    GLsizei stride;
    const GLvoid *offset;
};

class VertexBufferLayout {
private:
    std::vector<VertexBufferElement> elements;

public:
    VertexBufferLayout(){};
    ~VertexBufferLayout(){};
    void Push(VertexBufferElement element);
    const std::vector<VertexBufferElement> &GetElements() const;
};

#endif