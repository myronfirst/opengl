#ifndef INDEX_BUFFER_H
#define INDEX_BUFFER_H

#include "GL/glew.h"

#include <vector>

class IndexBuffer {
private:
    GLuint indexBufferObjectId;
    unsigned size;

public:
    IndexBuffer(const std::vector<unsigned> &bufferData);
    ~IndexBuffer();
    void Bind() const;
    void Unbind() const;
    unsigned GetSize() const { return size; };
};

#endif