#ifndef ERROR_H
#define ERROR_H

#include <string>

void MessageCallback(unsigned source, unsigned type, unsigned id,
                     unsigned severity, int length, const char *msg,
                     const void *data);
void ThrowError(const std::string &msg);

#endif