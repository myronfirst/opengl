#ifndef APP_H
#define APP_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class App {
private:
    GLFWwindow *window = nullptr;
    int windowWidth = 0;
    int windowHeight = 0;
    GLuint shaderProgram = 0;
    const char *vertexShaderPath = "src\\VertexShader.vert";
    const char *fragmentShaderPath = "src\\FragmentShader.frag";
    const char *texturePath = "resource\\dvbt.png";

public:
    App();
    ~App();
    void Run();

    int GetWindowWidth() const { return windowWidth; }
    int GetWindowHeight() const { return windowHeight; }
};
#endif